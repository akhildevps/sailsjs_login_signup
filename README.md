# Login & Signup - Sails Js


## Steps

* Install node.js - version 12
* Install sails.js
* Run npm install to install all the dependencies

## Configure database

* Install MongoDB 
* Run ' **npm install sails-mongo** ' in your app folder.
* Create collection name **sampledb**
* In your config/datastores.js file. 
    url: 'mongodb://root@localhost/sampledb' change to your mongoDB  connection
   
## Start project

* node app.js
* open Index.html